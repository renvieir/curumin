﻿using UnityEngine;
using System.Collections;

public class IndioControllerScript : MonoBehaviour {

    public float maxHeight = 10f;
    public float maxSpeed = 10f;
    bool facingRight = true;
    bool jumping = false;
    static int MAX_JUMPS = 2;

    Animator anim;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        float move = Input.GetAxis("Horizontal");

        float jump = Input.GetAxis("Vertical");


        anim.SetFloat("Speed",Mathf.Abs(move));

        rigidbody2D.velocity = new Vector2(move * maxSpeed, rigidbody2D.velocity.y);

        if (move > 0 && !facingRight)
            Flip();
        else if (move < 0 && facingRight)
            Flip();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }

        if (rigidbody2D.velocity.y > 0)
            jumping = false;
	}

    void Flip() 
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    public void Jump()
    {
        if (!jumping)
        {
            rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, -Physics.gravity.y);
        }
    }

}
